# Website

## Start your site

Run the development server:

```shell
npx docusaurus start
```

Your site starts at `http://localhost:3000`.

## To change

- `static/img/favicon.ico`
- `static/img/logo.svg`

### `docusaurus.config.js`

```js
  title: 'My Site',
  tagline: 'Dinosaurs are cool',
  url: 'https://icic.gitlab.io',
  baseUrl: '/doc-test/',
  favicon: 'img/favicon.ico',
  editUrl: 'https://github.com/facebook/docusaurus/edit/main/website/',

navbar{}
footer{}
```

### `package.json`

```json
  "name": "doc-test",
  "version": "0.0.0",
```
