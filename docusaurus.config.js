// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(module.exports = {
  title: 'Template - Docusaurus',
  tagline: 'Dinosaurs are cool',
  url: 'https://siakhooi.gitlab.io',
  baseUrl: '/template-docusaurus/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/',
          // Please change this to your repo.
          editUrl: 'https://gitlab.com/siakhooi/template-docusaurus/-/edit/main/',
        }
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      hideableSidebar: true,
      colorMode: {
        defaultMode: 'dark',
      },
      navbar: {
        title: 'Template - Docusaurus',
        // logo: {
        //   alt: 'Template - Docusaurus',
        //   src: 'img/logo.svg',
        // },
        items: [
          {
            type: 'doc',
            docId: 'tutorial/index',
            position: 'left',
            label: 'Tutorial',
          },
          //   {
          //     type: 'doc',
          //     docId: 'doc/index',
          //     position: 'left',
          //     label: 'Doc',
          //   },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          //     {
          //       title: 'Docs',
          //       items: [
          //         {
          //           label: 'Home',
          //           to: '/',
          //         },
          //         {
          //           label: 'Tutorial',
          //           to: '/tutorial/index',
          //         },
          //         {
          //           label: 'Doc',
          //           to: '/doc/index',
          //         },
          //       ],
          //     },
          {
            title: 'Reference',
            items: [
              {
                label: 'Docusaurus',
                href: 'https://docusaurus.io',
              }
            ],
          },
        ],
        //copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
      },
      // prism: {
      //   theme: require('prism-react-renderer/themes/github'),
      //   darkTheme: require('prism-react-renderer/themes/dracula'),
      // },
    }),
});
